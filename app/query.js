// const bcrypt = require('bcrypt-nodejs');
const mysql = require("mysql");
const uniqid = require("uniqid");

function devMode(x) {
  if (x === "server") {
    return mysql.createPool({
      acquireTimeout: 100000,
      connectionLimit: 200,
      host: "coinstory.c0sqiigeihke.ap-southeast-1.rds.amazonaws.com",
      user: "coinstory",
      password: "marcelc6363",
      database: "ahensibanget"
    });
  }
  if (x === "localhost") {
    return mysql.createPool({
      connectionLimit: 200,
      host: "localhost",
      user: "root",
      password: "",
      database: "ahensibanget"
    });
  }
}
const pool = devMode("server");

const callbackCheck = (callback, data) => {
  if (data === 400) {
    data = {
      meta: {
        code: 400
      }
    };
  }
  if (callback !== undefined) {
    callback(data);
  }
};

const mysqlQuery = (sql, callback) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(sql, function(err, result, fields) {
      if (err) throw err;
      callback(result, fields);
      connection.release();
    });
  });
};

const post = (payload, callback) => {
  //Switch
  let sql = "INSERT INTO `story` (title, story, timestring, sharecode) VALUE (?, ?, ?, ?)";
  let timestring = Math.round(new Date().getTime() / 1000);  

  //Query
  mysqlQuery(
    {
      sql: sql,
      timeout: 40000, // 40s
      values: [payload.title, payload.story, timestring, payload.sharecode]
    },
    x => {
      if (x.length !== 0) {
        let data = {
          meta: {
            code: 200
          }
        };
        callbackCheck(callback, data);
      }
    }
  );
};

const get = (payload, callback) => {
  //Switch
  let sql = "SELECT * FROM `story`";

  //Query
  mysqlQuery(
    {
      sql: sql,
      timeout: 40000, // 40s
      values: [payload.slug]
    },
    x => {
      console.log("Check Article", x);

      if (x.length === 0) {
        let data = {
          meta: {
            code: 200
          }
        };
        callbackCheck(callback, data);
      }
      if (x.length > 0) {
        let data = {
          meta: {
            code: 400
          }
        };
        callbackCheck(callback, data);
      }
    }
  );
};

module.exports = {
  post,
  get
};
