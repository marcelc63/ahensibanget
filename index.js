//Dependencies
let express = require("express");
let app = require("express")();
let http = require("http").Server(app);
let path = require("path");
let port = process.env.USER === "ec2-user" ? 3001 : 3000;
let query = require("./app/query.js");
const uniqid = require("uniqid");

var cors = require("cors");
app.use(cors());

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Route
app.get("/", function(req, res) {});
app.get("/test", function(req, res) {
  res.send("test");
});
app.post("/post", function(req, res) {
  console.log(req.body);
  let title = req.body.title;
  let story = JSON.stringify(req.body.story);  
  let sharecode = uniqid.time()
  query.post(
    {
      title: title,
      story: story,
      sharecode: sharecode
    },
    () => {}
  );
  res.json(sharecode)
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:" + port);
});
